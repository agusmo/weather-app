# Estructura de carpetas:

- `public`: static files - imágenes/iconos.
- `src`: Raíz del proyecto.
    -- `commons`: Carpeta contenedora de componentes reutilizables.
    -- `components`: Carpeta contenedora de los componentes.
    -- `screens`: Carpeta con las vistas de la web.
    -- `styles`: Carpeta con estilos globales.

# Levantar el proyecto:

1. Instalar dependencias: `npm install`
2. Levantar proyecto: `npm start`.
3. La aplicacion se levanta en el puerto [http://localhost:3000](http://localhost:3000).
