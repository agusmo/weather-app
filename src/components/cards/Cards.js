import React, { useEffect, useState } from "react";
import ForecastCard from "../forecastCard/ForecastCard";
import WeatherCard from "../weatherCard/WeatherCard";
import s from "./cards.module.scss";
import Loader from "../../commons/loader/Loader";

const Cards = ({ citySearch  }) => {

  return (  
    <div className={s.container}>
      <WeatherCard citySearch={citySearch}/>
      <ForecastCard citySearch={citySearch} />
    </div>
  );
};

export default Cards;
