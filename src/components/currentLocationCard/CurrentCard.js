import React, { useEffect, useState } from "react";
import axios from "axios";
import s from "./current.module.scss";
import Loader from "../../commons/loader/Loader";

const CurrentCard = () => {
  const [location, setLocation] = useState(null);
  const [forecast, setForecast] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [lat, setLat] = useState("");
  const [lon, setLon] = useState("");

  useEffect(() => {
    getLocation();
  }, []);

  const getLocation = () => {
    navigator?.geolocation?.getCurrentPosition((position) => {
      setLon(position.coords.longitude);
      setLat(position.coords.latitude);
    });
  };

  useEffect(() => {
    getCurrent(lon, lat);
  }, [lon, lat]);

  const getCurrent = async function () {
    try {
      const res = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=f3611f2d14174414e8cb12228e2ff905&units=metric`
      );
      setLocation(res.data);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getCurrentForecast(lon, lat);
  }, [lon, lat]);

  const getCurrentForecast = async function () {
    try {
      const res = await axios.get(
        `http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=f3611f2d14174414e8cb12228e2ff905&units=metric`
      );
      setForecast(res.data);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  return isLoading ? (
    <Loader />
  ) : (
    <div className={s.container}>
      <div className={s.subContainer}>
        <header> {location?.name}</header>
        <section className={s.mainCard}>
          <div className={s.subtitle}>
            <h2>Current Weather</h2>
            <h1>{Math.round(location?.main?.temp * 10) / 10}°</h1>
          </div>
          <img
            src={`http://openweathermap.org/img/w/${location?.weather[0].icon}.png`}
          />
          <p className={s.temp}>{location?.weather[0].description}</p>
          <div className={s.temp}>
            <p>Max: {Math.round(location?.main?.temp_max * 10) / 10}°</p>
            <p>Min: {Math.round(location?.main?.temp_min * 10) / 10}°</p>
          </div>
        </section>
      </div>
      <article className={s.cardContainer}>
        {[...Array(5)].map((e, i) => (
          <section className={s.card}>
            <h2>
              {new Date(forecast?.list[i * 9].dt_txt).toLocaleString("en-US", {
                weekday: "long",
              })}
            </h2>
            <img
              src={`http://openweathermap.org/img/w/${
                forecast?.list[i * 8].weather[0].icon
              }.png`}
            />
            <p>{forecast?.list[i * 8].weather[0].description}</p>
            <div>
              <p>
                Max: {Math.round(forecast?.list[i * 8].main.temp_max * 10) / 10}
                °
              </p>
              <p>
                Min: {Math.round(forecast?.list[i * 8].main.temp_min * 10) / 10}
                °
              </p>
            </div>
          </section>
        ))}
      </article>
    </div>
  );
};

export default CurrentCard;
