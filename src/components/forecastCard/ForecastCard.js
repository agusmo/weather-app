import React, { useEffect, useState } from "react";
import axios from "axios";
import s from "./forest.module.scss";

const ForecastCard = ({ citySearch }) => {
  const [forecast, setForecast] = useState(null);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    getForecast();
  }, []);

  const getForecast = async function () {
    try {
      const res = await axios.get(
        `http://api.openweathermap.org/data/2.5/forecast?q=${citySearch},&appid=f3611f2d14174414e8cb12228e2ff905&units=metric`
      );
      setForecast(res.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(true);
      console.log(error);
    }
  };

  return isLoading ? (
    ""
  ) : error ? null : (
    <article className={s.cardContainer}>
      {[...Array(5)].map((e, i) => (
        <section className={s.card}>
          <h2>
            {new Date(forecast?.list[i * 9].dt_txt).toLocaleString("en-US", {
              weekday: "long",
            })}
          </h2>
          <img
            src={`http://openweathermap.org/img/w/${
              forecast?.list[i * 8].weather[0].icon
            }.png`}
          />
          <p>{forecast?.list[i * 8].weather[0].description}</p>
          <div>
            <p>
              Max: {Math.round(forecast?.list[i * 8].main.temp_max * 10) / 10}°
            </p>
            <p>
              Min: {Math.round(forecast?.list[i * 8].main.temp_min * 10) / 10}°
            </p>
          </div>
        </section>
      ))}
    </article>
  );
};

export default ForecastCard;
