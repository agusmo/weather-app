import React, { useEffect, useState } from "react";
import axios from "axios";
import s from "./weather.module.scss";
import Loader from "../../commons/loader/Loader";

const WeatherCard = ({ citySearch }) => {
  const [weather, setWeather] = useState(null);
  const [isLoading, setLoading] = useState(true)
  const [error, setError] = useState(false)
      

  useEffect(() => {
    getWeather();
  }, []);

  const getWeather = async function () {
    try {
      const res = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?q=${citySearch},&appid=f3611f2d14174414e8cb12228e2ff905&units=metric`
      );
      setWeather(res.data);
      setLoading(false)
    } catch (error) {
      setLoading(false)
      setError(true)
      console.log(error);
    }
  };

  return (
    isLoading ? (  
      <Loader/>
      ) : error ? 
      null
     : (
    <div className={s.container}>
        <div className={s.subContainer}>
      <header> {weather?.name}</header>
      <section className={s.mainCard}>
        <div className={s.subtitle}>
          <h2>Current Weather</h2>
          <h1>{Math.round(weather?.main?.temp * 10) / 10}°</h1>
        </div>
        <img
          src={`http://openweathermap.org/img/w/${weather?.weather[0].icon}.png`}
        />
        <p className={s.temp}>{weather?.weather[0].description}</p>
        <div>
          <p className={s.temp}>Max: {Math.round(weather?.main?.temp_max * 10) / 10}°</p>
          <p className={s.temp}>Min: {Math.round(weather?.main?.temp_min * 10) / 10}°</p>
        </div>
      </section>
        </div>
    </div>
  ));
};

export default WeatherCard;