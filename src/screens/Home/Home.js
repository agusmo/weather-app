import React, { useState } from "react";
import s from "./home.module.scss";
import Cards from "../../components/cards/Cards";
import Layout from "../../commons/layout/layout";
import CurrentCard from "../../components/currentLocationCard/CurrentCard";

const Home = ({ isLoading }) => {
  const [citySearch, setCitySearch] = useState("");
  const [cards, setCards] = useState([]);

  const addCard = () => {
    cards.length < 5 &&
      setCards([
        ...cards,
        <Cards citySearch={citySearch} isLoading={isLoading} />,
      ]);
    setCitySearch("");
  };

  return (
    <Layout>
      <div className={s.container}>
        <section>
          <div className={s.header}>Weather App</div>
          <div className={s.search}>
          <input
            type="text"
            value={citySearch}
            onChange={(e) => setCitySearch(e.target.value)}
            placeholder="Type..."
          />
          <button disabled={!citySearch} onClick={addCard}
          className={!citySearch ? s.disabled : s.button}
          >
            Search
          </button>
          </div>
        </section>
        <CurrentCard isLoading={isLoading} />
        {cards}
      </div>
    </Layout>
  );
};

export default Home;
